# Prototype Boilerplate (Gulp)

A boilerplate for HTML prototypes using Gulp + ES6 + SASS + BrowserSync.

### SASS folder structure:
- 0_base: contains fonts, common_classes, variables, and basic CSS css_reset

### Usage:
- Install gulp globally: `npm install -g gulp`
- Clone: `git clone`
- Install NPM dependencies: `npm install`
- Run the server: `gulp`
