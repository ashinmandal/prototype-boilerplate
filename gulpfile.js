"use strict";

// Load plugins
const browsersync = require("browser-sync").create();
const sass = require("gulp-sass");
const gulp = require("gulp");
const babelify = require('babelify');
const browserify = require("browserify");
const source = require("vinyl-source-stream");
const buffer = require("vinyl-buffer");
const uglify = require("gulp-uglify");
const sourcemaps = require("gulp-sourcemaps");

// BrowserSync
function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: "./"
    },
    port: 3000
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// CSS task
function css() {
  return gulp
    .src("./scss/master.scss")
    .pipe(sass({ outputStyle: "expanded" }))
    .pipe(gulp.dest("./css/"))
    .pipe(browsersync.stream());
}

//Convert ES6 ode in all js files in src/js folder and copy to
//build folder as bundle.js
function js() {
  return browserify({
    debug: true,
    entries: ["./js/index.js"]
  }).transform("babelify", {
    global: true,
    presets: ["@babel/env"]
  })
    .bundle()
    .pipe(source("bundle.js"))
    // .pipe(buffer())
    // .pipe(sourcemaps.init())
    // .pipe(uglify())
    // .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest("./dist"));
}
// Uncomment the above to get minified JS and sourcemaps

// Watch files
function watchFiles() {
  gulp.watch("./scss/**/*", css);
  gulp.watch("./js/**/*", js);
  gulp.watch(
    ["./*.html", "./css/*.css", "./js/*.js"],
    gulp.series(browserSyncReload)
  );
}

// define complex tasks
const serve = gulp.series(css, js, gulp.parallel(watchFiles, browserSync));

exports.js = js;
exports.default = serve;
